#!/bin/bash

#description "Checks starting service"

# Location of lab checker scripts:
cd /root/lab-script-templates/router/

TEMPLATE="VirtualBox"
DMIDECODEVERSION="$(dmidecode -s bios-version | cut -d'-' -f1,2)"

if [ "$DMIDECODEVERSION" != "$TEMPLATE" ]; then

. /root/lab-script-templates/router/lab_init.sh
export LAB_USERNAME

# Lab checker scripts
#/root/vpnlab/router/check1.sh || true
#/root/vpnlab/router/check2.sh || true

fi
