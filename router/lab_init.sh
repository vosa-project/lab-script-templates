#!/bin/bash


# Set Desktop IP for the script to work

DESKTOPIP=

counter=10

while [ $counter -gt 0  ]; do

curl -H "accept: application/json" -X POST "$(dmidecode -s system-product-name)/labinfo?uuid=$(dmidecode -s system-version)" > /tmp/lab_data

        if [ $? -eq 0 ]; then
                break
        fi
        sleep 2
	$((counter--))
done


export LAB_NAME=$(cat /tmp/lab_data | jq -r '.lab.name')
export LAB_USERNAME=$(cat /tmp/lab_data | jq -r '.user.username')
export TA_KEY=$(cat /tmp/lab_data | jq -r '.lab.lab_token')
export VIRTUALTA_HOSTNAME=$(cat /tmp/lab_data | jq -r '.assistant.uri')
export USER_FULLNAME=$(cat /tmp/lab_data | jq -r '.user.name')
export USER_KEY=$(cat /tmp/lab_data | jq -r '.user.user_key')
export LAB_ID=$(cat /tmp/lab_data | jq -r '.lab.lab_hash')

cat > /root/lab-script-templates/lab.ini <<EOC
[LAB]
ta_key = $TA_KEY
virtualta_hostname = $VIRTUALTA_HOSTNAME
lab_id = $LAB_ID
EOC

# Send vTA url to Desktop

counter=10

while [ $counter -gt 0  ]; do

	ssh root@$DESKTOPIP "echo \"$VIRTUALTA_HOSTNAME\"/lab/\"$LAB_ID\"/\"$USER_KEY\" > /var/tmp/vta-url"

	if [ $? -eq 0 ]; then
		break
	fi
	sleep 2
	$((counter--))
done
