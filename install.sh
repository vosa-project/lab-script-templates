#!/bin/bash
# Author: Roland Kaur

echo "Desktop IP address:"
read DESKTOPIP

echo "Lab name in one word (no spaces):"
read LABNAME

case "$LABNAME" in
    *[[:space:]]*)
        echo "Lab name contains a space" >&2
        exit 0
        ;;
esac

if [ $LABNAME == "lab-script-templates" ]; then
    echo "Lab name cannot be lab-script-templates"
    exit 0
fi

echo "Assigning lab name"

mkdir /root/$LABNAME/
cd /root/$LABNAME

mv /root/lab-script-templates/* /root/$LABNAME/
sed -i "s/lab-script-templates/$LABNAME/g" router/startchecks.service router/startchecks.sh router/lab_init.sh router/objectivechecks.py

rm -r /root/lab-script-templates/

echo "Pinging Desktop..."
ping -q -c5 $DESKTOPIP > /dev/null

if [ $? -ne 0 ]; then
	echo "No connection to $DESKTOPIP"
	exit 1
else
	echo "Ping succesfull!"
fi

ssh -q root@$DESKTOPIP exit

if [[ $? -ne 0 ]]; then
        echo "Cannot connect to the Desktop via SSH"
        exit 2
else
        echo "SSH to Desktop is OK"
fi

echo "Checking for user student..."

ssh root@$DESKTOPIP "getent passwd student"

if [[ $? -ne 0 ]]; then
	echo "No user student $DESKTOPIP"
	exit 3
else
	echo "User student found in $DESKTOPIP"
fi

echo "Copying files..."

ssh root@$DESKTOPIP "mkdir -p /home/student/Desktop/"
scp desktop/run-virtualta.sh root@$DESKTOPIP:/home/student/
scp desktop/virtualta.desktop root@$DESKTOPIP:/home/student/Desktop/

echo "Changing permissions..."

ssh root@$DESKTOPIP "mkdir -p /home/student/.config/autostart/"
ssh root@$DESKTOPIP "chmod +x /home/student/run-virtualta.sh /home/student/Desktop/virtualta.desktop"
ssh root@$DESKTOPIP "chown student:student /home/student/ -R"
ssh root@$DESKTOPIP "ln -s /home/student/Desktop/virtualta.desktop /home/student/.config/autostart/virtualta.desktop"

echo "Installing jq"

apt-get install jq

sed -i "s/.*DESKTOPIP=.*/DESKTOPIP=\"$DESKTOPIP\"/g" router/lab_init.sh

echo "Creating and enabling systemd service"

chmod +x router/startchecks.sh router/lab_init.sh router/objectivechecks.py

mkdir -p /usr/local/lib/systemd/system/
cp router/startchecks.service /usr/local/lib/systemd/system/startchecks.service
systemctl daemon-reload
systemctl enable startchecks.service

echo "Done!"
